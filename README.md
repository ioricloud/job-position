# Aplicaçao Em GO

Criaçao desta aplicaçao foi para o teste de job position para Quero educaçao.

Conforme falei com o gestor da area, aplicaçao tem que gerar metricas para serem enviadas ao Prometheus.

O que deve ser dito da aplicaçao é que e uma aplicaçao feita em Go e que farei um docker para ela ser encapsulada.

---
### Definiçoes

Aplicaçao e um CRUD para criaçao usuario, no arquivo user.go, esta a Struct com criacao de usuario, e tem as functions para pegar usuarios, criar, atualizar e deletar usuarios.

Aplicacao esta setado para `/users` e esta setado para porta `:8080`, conforme foi pedido no endpoint `/metrics` esta apontado para expor para metricas para Prometheus.

---
### Endpoints

Para pegar todos os usuarios:

GET `http://<endereco-ip>:8080/users`

Para pegar um unico usuario: parametro id

GET `http://<endereco-ip>:8080/users/:id`

Para criar usuario:

POST `http://<endereco-ip>:8080/users`

Para atualizar usuario:

PUT `http://<endereco-ip>:8080/users/:id`

Para deletar usuario:

DELETE `http://<endereco-ip>:8080/users/:id`