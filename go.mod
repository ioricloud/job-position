module go-echo

go 1.15

require (
	github.com/labstack/echo-contrib v0.11.0 // indirect
	github.com/labstack/echo/v4 v4.5.0
)
