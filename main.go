package main

import (
	"go-echo/users"

	"github.com/labstack/echo-contrib/prometheus"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	e := echo.New()

	p := prometheus.NewPrometheus("echo", nil)

	p.Use(e)

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/users", users.GetUsers)
	e.GET("/users/:id", users.GetUser)
	e.POST("/users", users.CreateUser)
	e.PUT("/users/:id", users.UpdateUser)
	e.DELETE("/users/:id", users.DeleteUser)

	e.Logger.Fatal(e.Start(":8080"))
}
