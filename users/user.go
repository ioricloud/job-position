package users

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

type User struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

var Users = map[int]*User{}

var Seq = 1

func GetUsers(c echo.Context) error {
	return c.JSON(http.StatusOK, Users)
}

func GetUser(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	return c.JSON(http.StatusOK, Users[id])
}

func CreateUser(c echo.Context) error {
	u := &User{
		Id: Seq,
	}
	if err := c.Bind(u); err != nil {
		return err
	}
	Users[u.Id] = u
	Seq++
	return c.JSON(http.StatusCreated, u)
}

func UpdateUser(c echo.Context) error {
	u := new(User)
	if err := c.Bind(u); err != nil {
		return err
	}

	id, _ := strconv.Atoi(c.Param("id"))
	Users[id].Name = u.Name
	return c.JSON(http.StatusOK, Users[id])
}

func DeleteUser(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	delete(Users, id)
	return c.NoContent(http.StatusNoContent)
}
